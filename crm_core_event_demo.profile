<?php
/**
 * @file
 * CRM Core Event Demo install profile.
 */

/**
 * Implements hook_update_index().
 *
 * Triggered only on installation.
 */
function crm_core_event_demo_update_index() {
  // We need this to clear node_load cache after importing sample content in
  // order to prevent 'Notice: Undefined property: stdClass::$changed' msg.
  node_load_multiple(array(), array(), TRUE);
  variable_del('search_active_modules');
}
